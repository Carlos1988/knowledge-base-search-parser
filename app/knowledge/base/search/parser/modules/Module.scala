package knowledge.base.search.parser.modules

import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport
import knowledge.base.search.parser.utility.CentralConfiguration
import javax.inject.Singleton
import knowledge.base.common.library.zookeeper.ZookeeperService

class Module extends AbstractModule with AkkaGuiceSupport {
  import actors._
  val zkhost = Option(System.getenv("zkhost")).getOrElse("localhost")
  val zkport = Option(System.getenv("zkport")).getOrElse("2181")

  override def configure(): Unit = {
    bind(classOf[ZookeeperService]).toInstance(new ZookeeperService(zkhost.concat(":").concat(zkport)))
    bind(classOf[CentralConfiguration]).asEagerSingleton()
  }
}
