package knowledge.base.search.parser.utility

import knowledge.base.common.library.zookeeper.ZookeeperService

import scala.collection.immutable.Map
import play.api.Configuration
import javax.inject.Inject
import javax.inject.Singleton

object CentralConfiguration {
  private val CONST_CENTRAL_CONFIGURATION="centralconfiguration"
  private val CONST_SOLR=s"$CONST_CENTRAL_CONFIGURATION.solr"
  private val CONST_SOLR_PORT = s"$CONST_SOLR.port"
  private val CONST_SOLR_DOMAIN = s"$CONST_SOLR.domain"
  private val CONST_SOLR_DOC = s"$CONST_SOLR.document"
  private val CONST_SOLR_DOC_KNW_BASE = s"$CONST_SOLR_DOC.knowledge_base"
  private val CONST_SOLR_DOC_IDENTITY= s"$CONST_SOLR_DOC.identity"
  private val CONST_SOLR_DOC_SIGN = s"$CONST_SOLR_DOC.sign"
 
  object KEY extends Enumeration {
     type KEY = Value
     val SOLR_PORT = Value(s"$CONST_SOLR_PORT")
     val SOLR_DOMAIN = Value(s"$CONST_SOLR_DOMAIN")
     val SOLR_DOC_KNW_BASE = Value(s"$CONST_SOLR_DOC_KNW_BASE")
     val SOLR_DOC_IDENTITY = Value(s"$CONST_SOLR_DOC_IDENTITY")
     val SOLR_DOC_SIGN = Value(s"$CONST_SOLR_DOC_SIGN")
  } 
}

@Singleton
class CentralConfiguration @Inject()(zookeeperService: ZookeeperService, appConf: Configuration) {
    private var configs = Map[CentralConfiguration.KEY.Value, String]()
 
  
  private def gatherConfiguration(): Unit = {
    configs = CentralConfiguration.KEY.values.foldLeft(Map[CentralConfiguration.KEY.Value, String]()){
        (accum, key) => {
          val node_key = key.toString().concat(".key")
          val node_default = key.toString().concat(".default")
          accum + ((key, zookeeperService.getData(appConf.get[String](node_key)).getOrElse("")))
        }
      }
  }
  
  def get(key: CentralConfiguration.KEY.Value): Option[String] = {
     if(configs.isEmpty){
       this.gatherConfiguration();
     }
     configs.get(key)
  }
}