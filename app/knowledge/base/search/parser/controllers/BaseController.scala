package knowledge.base.search.parser.controllers

import javax.inject._

import play.api.{Configuration, Logger}
import play.api.libs.json.{JsObject, JsString, JsValue, Json}
import play.api.libs.ws._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import knowledge.base.search.parser.utility.CentralConfiguration
import knowledge.base.common.library.solr.SolrConnectionFactory
import knowledge.base.search.parser.solr.SolrClient
import play.api.libs.json.Reads
import play.api.libs.json.JsError
import knowledge.base.search.parser.model.Knowledge


class BaseController @Inject()(cc: ControllerComponents)(implicit ec: ExecutionContext) extends AbstractController(cc)  {
    import knowledge.base.search.parser.model.Model.Reads.knowledgeReads
  
  
    def validateJson[A: Reads] = parse.json.validate(_.validate[A].asEither.left.map(e =>BadRequest(JsError.toJson(e))))

    protected val logger = Logger(this.getClass)
}