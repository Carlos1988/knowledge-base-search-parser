package knowledge.base.search.parser.controllers

import javax.inject._

import play.api.{Configuration, Logger}
import play.api.libs.json.{JsObject, JsString, JsValue, Json}
import play.api.libs.ws._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import knowledge.base.search.parser.utility.CentralConfiguration
import knowledge.base.common.library.solr.SolrConnectionFactory
import knowledge.base.search.parser.solr.SolrClient
import play.api.libs.json.Reads
import play.api.libs.json.JsError
import knowledge.base.search.parser.model.Knowledge

@Singleton
class KnowledgeController @Inject()(solrClient: SolrClient, cc: ControllerComponents)(implicit ec: ExecutionContext) extends BaseController(cc)  {
    import knowledge.base.search.parser.model.Model.Reads.knowledgeReads
    import knowledge.base.search.parser.model.Model.Writes.knowledgeWrites
    
    def index = Action(validateJson[Knowledge]) { request =>
      try {
        solrClient.index(request.body)
        Ok(Json.toJson(request.body)) 
      } catch {
        case _: Throwable =>
          InternalServerError("""{'message': 'Failed to add a knowledge.'}""")  
      }
    }
}