package knowledge.base.search.parser.solr

import knowledge.base.common.library.solr.SolrConnectionFactory
import knowledge.base.common.library.solr.SolrConnectionParameters
import knowledge.base.common.library.solr.SolrCommonDocument
import javax.inject.Singleton
import javax.inject.Inject
import knowledge.base.search.parser.utility.CentralConfiguration
import scala.collection.JavaConverters
import knowledge.base.search.parser.model.Knowledge
import org.apache.solr.common.SolrDocumentList
import org.apache.solr.client.solrj.SolrQuery

@Singleton
class SolrClient @Inject()(centralConfig: CentralConfiguration){
  val solrDocumentKnowBase = new SolrCommonDocument(centralConfig.get(CentralConfiguration.KEY.SOLR_DOC_KNW_BASE).get)
  val solrDocumentIdentity = new SolrCommonDocument(centralConfig.get(CentralConfiguration.KEY.SOLR_DOC_IDENTITY).get)
  val solrDocumentSign = new SolrCommonDocument(centralConfig.get(CentralConfiguration.KEY.SOLR_DOC_SIGN).get)
  val domain = centralConfig.get(CentralConfiguration.KEY.SOLR_DOMAIN).get
  val port = centralConfig.get(CentralConfiguration.KEY.SOLR_PORT).get
  val solrParmsKnowBase = new SolrConnectionParameters(false, domain, port, solrDocumentKnowBase)
  
 
  def index(knowledge: Knowledge): Boolean = {
    //Add document for Identity
    //Add a document for Sign
    val solrClient = SolrConnectionFactory.getConnection(solrParmsKnowBase);
    val knowledgeMap = new java.util.HashMap[String, java.lang.Object]()
    knowledgeMap.put("idenity", knowledge.identity.id.get)
    knowledgeMap.put("signs", knowledge.signs.map(_.id.get))
    return solrClient.indexAny(knowledgeMap)
  }
 
}