package knowledge.base.search.parser.model

import java.util.UUID
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class Identity(id: Option[UUID], description: String)
case class Sign(id: Option[UUID], description: String)
case class Knowledge(identity: Identity, signs: Seq[Sign])

object Model {
  
  import Reads._
  import Writes._
  
  private val termBuilder = (JsPath \ "id").readNullable[UUID] and
    (JsPath \ "description").read[String]
 
  private val termWriter = (JsPath \ "id").writeNullable[UUID] and
    (JsPath \ "description").write[String]
 
  
  object Reads {
    implicit val identityReads: Reads[Identity] = (termBuilder)(Identity.apply _)
    implicit val signReads: Reads[Sign] = (termBuilder)(Sign.apply _)
    
    val knowledgeBuilder = (JsPath \ "identity").read[Identity] and
    (JsPath \ "signs").read[Seq[Sign]]

    implicit val knowledgeReads: Reads[Knowledge] =(knowledgeBuilder)(Knowledge.apply _)
  }
 
   object Writes {
    implicit val identityWrites: Writes[Identity] = (termWriter)(unlift(Identity.unapply))
    implicit val signWrites: Writes[Sign] = (termWriter)(unlift(Sign.unapply))
 
    val knowledgeWriter = (JsPath \ "identity").write[Identity] and
    (JsPath \ "signs").write[Seq[Sign]]
  
    implicit val knowledgeWrites: Writes[Knowledge] =(knowledgeWriter)(unlift(Knowledge.unapply))    
  }
}
